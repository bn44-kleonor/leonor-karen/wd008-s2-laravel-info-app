<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Post::insert([
        	['title' => 'Post 1', 'body' => 'This is the post 1 body'],
        	['title' => 'Post 2', 'body' => 'This is the post 2 body'],
        	['title' => 'Post 3', 'body' => 'This is the post 3 body']
        ]);
    }
}


