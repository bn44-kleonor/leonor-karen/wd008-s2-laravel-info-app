<?php

use Illuminate\Database\Seeder;
use App\Comment;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Comment::insert([
        	['body' => 'This is the post 1 body'],
        	['body' => 'This is the post 2 body'],
        	['body' => 'This is the post 3 body']
        ]);
    }
}
