<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">

    </head>
    <body>
    	@include('partials.navbar')
    	<div class="container">
    		<div class="row">
    			<div class="col">
        			@yield('content')
    			</div>
    		</div>
    	</div>
    	@include('partials.footer')

    	<!-- script -->
    	<script src="{{ asset('/js/jquery-3.4.1.min.js') }}"></script>
    </body>
</html>
